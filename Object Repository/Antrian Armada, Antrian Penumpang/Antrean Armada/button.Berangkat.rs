<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>button.Berangkat</name>
   <tag></tag>
   <elementGuidId>1c33d212-e1f4-4f85-8cb5-0f999f387ebe</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.Button</value>
      <webElementGuid>fd60c6d8-0e2f-4b37-9000-886cfa3b106b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>a60acb06-a51a-4bf2-8cea-bf58d98aaa33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>{noAntrean}</value>
      <webElementGuid>1d235c6f-bade-489e-a0fd-d4dbde65517e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>id.bluebird.vsm.pangkalan.stage:id/btn_assign_car</value>
      <webElementGuid>1dd4076b-c021-4276-a87c-922a8ce3a6f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>id.bluebird.vsm.pangkalan.stage</value>
      <webElementGuid>9673e060-a6b1-43fd-8c1c-37263ee70d82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>c596568a-3d09-4b5e-95ef-8857def17a8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>0a51e392-5ee2-491f-bcc1-2bf994f2ea12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>5e86f572-c0e9-4b19-a519-dbec2e1a899b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7ff03539-4b11-40e6-a567-0f1ed359c2c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>acd6f6b3-1f53-46f3-aeb6-99cb7eb908d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>dfaa66cd-f745-4f0f-abe9-63a6a91474fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>f428fc85-8501-4c8f-a8b2-c2efc3a96f4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>8357e00f-875d-4750-8f04-2d71c56c32b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>9564485f-7c43-4269-84ec-08a28526643c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>fbe73f4b-9441-4f7d-8fea-62ae8265a8a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>728</value>
      <webElementGuid>f163d822-d663-48f9-8641-01a21c19e019</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>1160</value>
      <webElementGuid>fd90c5aa-7f78-4c19-afe2-4696f7d8d2a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>310</value>
      <webElementGuid>a22db606-7ec6-4a1e-90b8-777de57f1b48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>105</value>
      <webElementGuid>fe7a64e6-2736-4f0c-916b-54b119d1fbc6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>
      <value>[728,1160][1038,1265]</value>
      <webElementGuid>59cc6d2f-dddd-4fac-889a-4dc628355e05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>52a4c467-5841-4904-b810-9c769377410e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/androidx.drawerlayout.widget.DrawerLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.LinearLayout[2]/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[1]/android.widget.LinearLayout[1]/android.widget.Button[1]</value>
      <webElementGuid>cf508911-4cf8-489c-a212-988aee59bd93</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.Button' and @resource-id = 'id.bluebird.vsm.pangkalan.stage:id/btn_assign_car']</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
