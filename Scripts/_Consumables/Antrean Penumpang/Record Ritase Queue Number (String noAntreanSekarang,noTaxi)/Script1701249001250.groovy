import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def noAntreanSekarangExpected = GlobalVariable.DOAntreanSekarang

String noTaxiExpected = GlobalVariable.DONoTaxi

String ActualResult = Mobile.getText(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/textView.antreanSekarang'), 
    0)

Mobile.verifyMatch(ActualResult, noAntreanSekarangExpected, false)

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/button.NextAntrean'), 0)

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Pencatatan Ritase/button.PilihNoLambung'), 
    0)

Mobile.setText(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Pencatatan Ritase/Pilih No Lambung/editText.CariNoLambung'), 
    noTaxiExpected, 0)

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Pencatatan Ritase/Pilih No Lambung/button.NoLambung', 
        [('noTaxi') : noTaxiExpected]), GlobalVariable.DOTimeOut)

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Pencatatan Ritase/Pilih No Lambung/button.Pilih'), 
    0)

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Pencatatan Ritase/button.CatatRitase'), 
    0)