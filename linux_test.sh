#!/usr/bin/env bash
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
export APPIUM_HOME="/var/lib/jenkins/appium" 
export PATH=$JAVA_HOME/bin:$PATH

PROJECT="$PWD/$1"

# java -version
/var/lib/jenkins/kse/katalonc -noSplash -runMode=console -projectPath="$PROJECT" -retry=0 -testSuitePath="$2" -browserType="Android" -deviceId="$3" -executionProfile="staging" -apiKey="$4" --config -proxy.auth.option=NO_PROXY -proxy.system.option=NO_PROXY -proxy.system.applyToDesiredCapabilities=true