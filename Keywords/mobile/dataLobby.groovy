package mobile

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class dataLobby {

	@Keyword
	def location() {

		def testData = findTestData('Data Files/data lobby')
		def totalData = testData.getRowNumbers()
		String expectedResultLocation = GlobalVariable.DOLocation
		def isMatched = false
		def i = 1

		while ((isMatched == false) && (i < totalData)) {
			if ((testData.getObjectValue('Location', i)).equals(expectedResultLocation)) {
				Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/button.Location', [('location') : expectedResultLocation]),
				GlobalVariable.DOTimeOut)
				isMatched = true
			}
			i++
		}

		if (isMatched == true) {
			KeywordUtil.logInfo('Location Is Found')
		} else {
			KeywordUtil.logInfo('Location Is Not Found')
		}
	}

	@Keyword
	def SubLocation() {

		def testData = findTestData('Data Files/data lobby')
		def totalData = testData.getRowNumbers()
		String expectedResultSubLocation = GlobalVariable.DOSublocation
		def isMatched = false
		def i = 1

		while ((isMatched == false) && (i < totalData)) {
			if ((testData.getObjectValue('SubLocation', i)).equals(expectedResultSubLocation)) {
				Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/subButton.Sublocation', [('sublocation') : expectedResultSubLocation]),
				GlobalVariable.DOTimeOut)
				isMatched = true
			}
			i++
		}

		if (isMatched == true) {
			KeywordUtil.logInfo('Location Is Found')
		} else {
			KeywordUtil.logInfo('Location Is Not Found')
		}
	}
}
